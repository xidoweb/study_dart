import { useState } from 'react';
import AppRouter from "components/Router";
// import fbase from 'fbase';
import { authService } from 'fbase';


function App() {  
  const [isLoggedIn, setIsLoggedIn] = useState(authService.currentUser);
  return ( <>
    <AppRouter isLoggedIn={isLoggedIn} /> 
    <footer>&copy; {new Date().getFullYear()} xido study </footer>
    </>)}

export default App;
